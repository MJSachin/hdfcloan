package com.whizdm;

import android.app.Application;
import com.whizdm.analytics.SimpleTracker;

/**
 * Created by vela on 19/06/14.
 */
public class HdfcApp extends Application {
    private static final String TAG = "HdfcApp";

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize the library
        WhizDMLibrary.initialize(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
